#!/usr/bin/env python
# -*- coding: utf-8 -*-


## Les calories

Ingredients = ['Sa', 'Kz', 'Mo', 'Ke', 'Og']
Cals = [140, 120, 20, 80, 40]

no_menu = 1

noms = "\nMenu"
for ing in Ingredients:
    noms += '\t' + ing
noms += "\tCalories"

print(noms)

for sossis in [0, 1]:
    for kramp in [0, 1]:
        for mout in [0, 1]:
            for ket in [0, 1]:
                for oignon in [0, 1]:
                    Ings = [sossis, kramp, mout, ket, oignon]
                    cals = 0
                    ligne = str(no_menu)
                    for k in range(len(Ings)):
                        cals += Ings[k]*Cals[k]
                        ligne += f"\t{Ings[k]}"
                    print(ligne + '\t' + str(cals))
                    no_menu += 1
