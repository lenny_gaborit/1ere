#1 Les importations
from typing import List

#2 Les fonctions utiles
def momMax(xs: List[int]) -> int:
    assert xs != [], "Une liste vide n'a pas de max"
    max_actuel = xs[0] # au départ le + gd est le 1er
    for x in xs:
        if x > max_actuel:
            max_actuel = x
    return max_actuel

#3 Programma principal
## Entrée
ma_liste = [12, 34, 54, 23, 199, 345, 221, -12, 0, 55]

## Traitement
m = monMax(ma_liste)

## Sortie
print(m)
