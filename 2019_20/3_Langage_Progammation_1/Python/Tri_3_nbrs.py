import easygui as eg

#### TRI DE TROIS NOMBRES

### ENTRÉES

## version texte
#a = float(input('\nDonnez un premier nombre : '))
#b = float(input('Donnez un second nombre différent du premier : '))
#c = float(input('Donnez un dernier nombre différent des deux premiers : '))

## version easygui
champs = ['1er nombre', '2nd nombre', '3e nombre']
msg = 'Entrez vos trois nombres'
titre = 'Classons trois nombres'
a, b, c = eg.multenterbox(msg, titre, champs)
a, b, c = float(a), float(b), float(c)

### VÉRIFICATIONS

assert a != b and a != c and b != c, "Les nombres doivent être différents"

### TRAITEMENT

grand = max(a, b)
petit = min(a, b)

if c > grand:
    grand , moyen = c, grand
elif c < petit:
    petit, moyen = c, petit
else:
    moyen = c

res = f"Le classement dans l'ordre croissant est {petit} < {moyen} < {grand}"

### SORTIE

## version texte
#print(res)

## easygui
eg.msgbox(res, title = "Classement", ok_button = "Quitter")


