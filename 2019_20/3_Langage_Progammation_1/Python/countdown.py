# -*- coding: utf-8 -*-


### Compte à rebours étoilé
import time
import os


total = int(input('\nCombien de secondes ? '))

for sec in range(total, 0, -1):
    print(str(sec) + ' @ '*sec)
    os.popen('cvlc --play-and-exit --quiet ../IMG/tick.wav')
    time.sleep(1)
print('\nBOUM !')
os.popen('cvlc  --play-and-exit --quiet ../IMG/explosion.wav')
