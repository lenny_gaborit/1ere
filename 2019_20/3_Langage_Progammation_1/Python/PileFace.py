# -*- coding: utf-8 -*-
#!/usr/bin/env python


####
##        PILE OU FACE
#########################################

import easygui as eg
import random as rd

## ENTRÉE
#choix: int = int(input("Pile(tapez 1) ou Face(tapez 0) ?"))
choix: int = int(eg.ynbox(msg = 'Pile ou Face ?', choices = ('Pile', 'Face'), image = '../IMG/Pile.png' )  )
    
##  TRAITEMENT
tirage:int = rd.randint(0,1)
res: str = "Tu as le Bac !" if choix == tirage else "Va au Pellerin !"
    
##  SORTIE
#print(res)
eg.msgbox(res, title = "Résultat", ok_button = "Quitter")
