# Environnement de travail

## Visual studio

Au lycée comme chez vous, nous travaillons sous GNU-Linux. Au lycée, la suite
Microsoft  est  omniprésente  donc  nous travaillerons  avec  l'IDE  (Integrated
Development Environment)  **Visual Studio**  dans sa  *community edition*  qui a
l'avantage d'être gratuite. 


## Création/ouverture d'un fichier Python

Vous  pouvez ouvrir  Visual Studio  en cliquant  sur son  icône de  lancement et
ouvrir ou  créer un fichier  depuis le  menu ou avec  `C-S` ou `C-O`  (`C` comme
`Ctrl`, `S` comme *save* et `O` comme `open`).


Vous vous placez sinon dans votre répertoire de travail et vous lancez `code`

```console

$ cd ./chemin/vers/répertoire/de/travail
$ code fichier.py
```


Une magnifique fenêtre s'ouvre :

![capture visual code](./IMG/code.png)


Il ne reste plus qu'à lancer la bête dans le terminal qui se situe en-dessous :


![capture visual code](./IMG/code2.png)

```console
$ python3 fichier.py
```

Il faut que  votre fichier est une entrée depuis  l'entrée standard (`stdin`) et
une sortie vars la sortie standard (`stdout`).
