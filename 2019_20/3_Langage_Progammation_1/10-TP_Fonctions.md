# TP : Fonctions

## Travaux précédents

Vous pourrez revoir  les programmes créés dans les TP  précédentes et introduire
des fonctions pour faciliter/améliorer leur utilisation (notamment le Poker).


## Vocabulaire


```python
def fonction(mot: str, lettre: str) -> int:
   compteur = 0
   for caractere in mot:
      if caractere == lettre:
          compteur = compteur + 1
   return compteur

n = fonction('Trololo', 'o')
```

Dans le script ci dessus :

1. Quel est le nom et la signature de la fonction ?
2. Quels sont les paramètres de la fonction ?
3. Quels sont les arguments de ce code ?
4. Quelles sont les variables locales à la fonction ?
5. Quel est le type de `n`
6. Quelle sera la valeur de `n` après l'exécution de ce script ?
7. Construisez  des phrases  décrivant la dernière  ligne en  utilisant *appel*,
   *appelant*, *appelé*, *argument*, *passe*,...

## Double

Considérons a fonction

```python
def double(s):
    return s + s
```

Qu'affiche le programme suivant ?

```python
s = "Salut"
s = double(s)
t = "Bye"
t = double(double(double(t)))
```

## Max

Créez une fonction `monMax(xs: List[int]) -> int` qui renvoie le maximum d'une
liste d'entiers.

## Portée de variable

Sans exécuter le code  suivant, dites combien de fois la  boucle `while` va être
itérée ?

```python

def cube(i):
	i = i*i*i
	
i = 10
while i < 1000000:
	cube(i)
	i += 1
```

Et si on remplaçait l'appel à `cube(i)` par `i = i*i*i` ?


## Hasard

Créer une fonction qui prend en argument un entier naturel `n` et qui renvoie un
tableau de longueur `n` remplie de nombres aléatoires (flou...).

## Mélange

1. On voudrait échanger deux éléments d'un tableau (`list`) donné en
   argument. Cette  demande est volontairement  floue. Comment y répondre  avec une
   fonction pure / une fonction impure ?

2. On  voudrait ensuite  utiliser cette  fonction pour  mélanger  un tableau
   donné en argument. Comment procéder purement/impurement ?
   

## Facture

On  dispose d'une  liste de  produits,  d'une liste  des prix  unitaires de  ces
produits  et d'une  liste  des quantités  achetées de  ces  produits. Créez  une
fonction qui calcule le prix total de ces achats.

Créez un programme interactif qui propose des produits avec leurs prix unitaires
et demande  quelle quantité on  souhaite acheter et renvoie  le prix à  payer et
affiche le détail de la facture.

Créez  un programme  interactif  qui demande  des noms  de  produits, leur  prix
unitaire, la quantité achetée  et renvoie  le prix à  payer et
affiche le détail de la facture.


Proposez des variantes : remises en cas d'achats nombreux, TVA,...

## Histogramme

Créez une fonction qui prend un tableau `tab` d'entiers en paramètre et qui renvoie un
nouveau tableau dont la `i`-ème cellule  contient le nombre de fois que l'entier
`i` apparaît dans le tableau `tab`.

## Mini-projet musical : partie 1

Pour les personnes  à l'âme musicienne, voici un moyen  assez simple de composer
de        la       musique        en        produisant       des        fichiers
[MIDI](https://fr.wikipedia.org/wiki/Musical_Instrument_Digital_Interface)
(Musical Instrument Digital Interface).

Il faut pour cela installer timidity sur votre système :

```console
$ sudo apt-get install timidity
```

Puis installer la bibliothèque Python `pyknon`

```console
$ pip3 install pyknon
```

Les imports de ce projet seront:

```python
from pyknon.genmidi import Midi
from pyknon.music import NoteSeq
import subprocess
```

Il faudra ensuite utiliser les notations en lettres anglaises :

La est A, Si est B, etc.

On fait suivre la note de l'inverse de  son temps. Par exemple pour un la noir :
`a4`.

Les silences sont notés `R` suivi de leur durée.

Ensuite on peut monter d'une octave avec `'` et descendre d'une octave avec `,`.

Voici un morceau musical :

```python
riff_sat: str = "b4, b4, R8 b8, c#8' d4' R8 d8' d8' c#8' c#4' "
```


Différentes  fonctions  de  la  bibliothèque `pyknon`  vont  nous  permettre  de
produire des fichiers midi  et on écoute le morceau en  envoyant une commande au
terminal via la méthode `check_call` de la bibliothèque `subprocess`:

```python
riffs = NoteSeq(3*riff_sat) # Transformation au format pyknon des séquences de notes
midi_gen = Midi(1, tempo=130, instrument=29) # choix des instruments, du tempo midi
midi_gen.seq_notes(riffs, track=0) # conversion au format midi
midi_gen.write("demo.midi") # écriture du fichier midi
subprocess.check_call(["timidity", "demo.midi"]) # le morceau est joué
```

Essayez à présent de rendre intéractif la création de musique via `pyknon` en
facilitant la vie de l'utilisateur.




## Mini-projet musical : partie 2


Il existe un fameux morceau composé en 1747 par Johann Sebastian BACH : le *canon à
cancrizans* qui est interprété par deux musiciens, le second lisant la partition
depuis la fin, de droite à gauche !

Le voici :

```console
"c2' eb2 g2 ab2 b2, r4 g2' f#2 f2 e2 eb2 d4 db4 c4 b4, g4 c4' f4 eb2 d2 c2 eb2 g8 f8 g8 c8'' g8' eb8 d8 eb8 f8 g8 a8 b8 c8'' eb8' f8 g8 ab8 d8 eb8 f8 g8 f8 eb8 d8 eb8 f8 g8 ab8 bb8 ab8 g8 f8 g8 ab8 bb8 c8'' db8 bb8' ab8 g8 a8 b8 c8'' d8 eb8 c8 b8' a8 b8 c8'' d8 eb8 f8 d8 g8' d8'' c8 d8 eb8 f8 eb8 d8 c8 b8' c4'' g4' eb4 c4"
```

L'idée est de  le faire interpréter par une machine  en utilisant `pyknon`. Pour
cela, il faut  regarder d'un peu plus près la "fonction" (en
attendant d'en savoir un peu plus sur la programmation orientée objet)
`Midi`. Il y a trois "paramètres" :
* `number_tracks` qui  donne le nombre de pistes MIDI  utilisées. Par défaut cet
  argument vaut 1. Ici, nous aurons besoin de deux pistes.
* `tempo` qui  vaut entre 0 et  127 battements par minute. La  valeur par défaut
  est 60.
* `intrument` qui définit l'instrument utilisé parmi les 128 disponibles,
  numérotés de 0 à 127. L'intrument par défaut est 0 (piano).


Quant  aux deux  méthodes  qu'on  peut appliquer  à  ces  "objets", voici  leurs
descriptions.

`seq_notes(sequence de notes, piste, depart)` admet trois paramètres :
* `note_seq` : une suite de notes du type `NoteSeq`
* `track` qui détermine le numéro de la  piste à laquelle la suite de notes sera
  attribuée. Attention, la première piste porte le numéro 0. C'est d'ailleurs la
  valeur par défaut de cet argument.
*  `time` indique  le nombre  de temps  après lesquels  la suite  de notes  sera
  jouée. La valeur par défaut est bien sûr 0.
  
L'autre  fonction est  `write` qui  n'admet qu'un  paramètre qui  est le  nom du
fichier MIDI cible  dans lequel sera enregistré le morceau.  C'est une chaîne de
caractères.

Il faut  donc créer un  fichier MIDI qui joue  la séquence précédente  selon les
recommandations de Bach.  Traditionnellement, la seconde voix  est baissée d'une
octave pour mieux la distinguer de la première.


Les méthodes `join` et `split` pourraient servir.

## Mini-projet Calendrier et découverte de `sys.argv`

On voudrait obtenir le calendrier d'un  mois donné d'une année donnée en lançant
simplement dans un terminal :

```console
$ python3 calendrier.py 2 2020
```
pour avoir le calendrier du mois de février 2020 :

```console
  Février 2020      
Di Lu Ma Me Je Ve Sa  
                   1  
 2  3  4  5  6  7  8  
 9 10 11 12 13 14 15  
16 17 18 19 20 21 22  
23 24 25 26 27 28 29  
```

Pour cela, on peut utiliser `argv` de la bibliothèque `sys` qui renvoie la liste
des arguments donnés en ligne de commande.

Par  exemple, ici,  si l'on  a  entré `  python  calendrier.py 2  2020` dans  un
terminal, la liste `argv` va contenir :

```python
["calendrier.py", "2", "2020"]
```

Donc le mois sera `int(sys.arg[1])` et l'année sera `int(sys.arg[2])`.

N'oubliez pas de traiter les années bissextiles...

Il va d'abord falloir s'occuper de savoir quel jour de la semaine est telle date
du calendrier.

L'allemand Zeller a proposé une formule en 1885.

On note  *m* le numéro  du mois  à partir de  janvier, *s* le  numéro du
siècle, *a*  le numéro de l'année dans  le siècle *s*, *j*  le numéro du
jour. Pour 1492, *s* = 14 et *a* = 92.


En fait, *m* doit être modifié selon le tableau suivant:


|Mois| Jan. | Fév.  | Mars | Avril | Mai | Juin | Juil.  | Août | Sep. | Oct. | Nov. | Déc.|
|----|------|-------|------|-------|-----|------|--------|------|------|------|------|-----|
|Rang |13* |14* | 3 |4 |5 |6 |7 |8 |9 |10 |11 |12|
	

Les mois marqués d'une astérisque comptent pour l'année précédente.



Le nom du jour (0 correspondant à samedi) est alors déterminé en prenant
le reste (positif!) dans la division par 7 de *nj* donné par la formule:


$`nj=j+a+(a\%4)+(26(m+1)//10)+(s//4)-2s `$

Ceci est  valable à partir du 15  octobre 1582, date à  laquelle le pape
Grégoire XIII a modifié le calendrier: on est passé du jeudi 4 octobre
1582  au  vendredi  15  octobre  1582 (en  Grande-Bretagne  il  a  fallu
attendre 1752, au Japon en 1873, en Russie 1918, en Grèce 1924).

 L'année est alors de 365 jours, sauf quand elle est bissextile, i.e., divisible par 4, sauf les années séculaires (divisibles par 100), qui ne sont bissextiles que si divisibles par 400.
 

Avant le changement, il faut remplacer $`(s//4)-2s`$
par $`5-s`$.

Pour quelques détails sur la formule de Zeller voir  cette [adresse](http://cosmos2000.chez.com/Vendredi13/ZellerMethode.html)



Quel jour est né le petit Jésus? Quel jour a été prise la Bastille?



## Découverte des images et des matrices

### PGM

Nous avions déjà découvert le format PGΜ au début d'année : 

[exemple de fichier PGM](https://gitlab.com/lyceeND/1ere/blob/master/2019_20/1_Archi_1/base1.ipynb)

C'est une *image matriciellle* encore appellée *carte de points* (**bitmap**).

Une matrice est une **grille** de **pixels** (*picture element*) qui est l'unité
minimale colorisable  sur un écran.  Une résolutiuon de disons  900×720 signifie
par exemple  que lécran a  900 pixels en largeurs  et 720 en  hauteur, l'origine
étant en haut à gauche.

### Échelle de gris

C'est l'hiver et nous oublions les couleurs. Voici donc une *échelle de gris*:

![grayscale](./IMG/grayscale.png "grayscale")

Les niveaux de gris sont codé sur un octet : il y en a donc...?

La bibliothèque `matplotlib.pyplot` permet  de représenter facilement un tableau
de *pixels* colorés.

Un "écran" est une **liste de lignes de pixels**.

Ici, on trace un tableau avec juste une ligne de 25 pixels, chaque pixel ayant
pour  niveau  de   gris  son  rang  multiplié  par  10.   Une  construction  par
compréhension est appropriée :


```python
import matplotlib.pyplot as plt

ligne_grise = [ [10*no_pixel for no_pixel in range(26)] ]

plt.imshow(ligne_grise, cmap='gray')

plt.show()
```

![ligne1](./IMG/ligne1.png)

Expliquez pourquoi on  obtient cette image ? Quelle est  la *résolution* (nombre
de pixels  par unité de  longueur) ? Comment est  calculée la couleur  de chaque
pixel ? Comment lit-on sa position ?


Ou deux lignes grises :

```python
deux_lignes_grises = [
    [10*no_pixel for no_pixel in range(26)],
    [250 - 10*no_pixel for no_pixel in range(26)]
]

plt.imshow(deux_lignes_grises, cmap='gray')

plt.show()
```
![deux_lignes](./IMG/lignes_2.png)

Expliquez pourquoi on  obtient cette image ? Quelle est  la *résolution* (nombre
de pixels  par unité de  longueur) ? Comment est  calculée la couleur  de chaque
pixel ? Comment lit-on sa position ?



On peut alors s'amuser avec par exemple un écran de résolution 16×15 :

```python
image = [[no_col*no_ligne for no_col in range(16)] for no_ligne in range(15)]
```

![cercles](./IMG/cercles.png)


Expliquez pourquoi on  obtient cette image ? Quelle est  la *résolution* (nombre
de pixels  par unité de  longueur) ? Comment est  calculée la couleur  de chaque
pixel ? Comment lit-on sa position ?


### À chercher :

Tracer cette croix :

![croix](./IMG/croix.png)


et une grille *aléatoire* similaire à celle-ci :

![alea](./IMG/alea.png)




## Mini-projet : manipulation d'images

Travaillons maintenant avec une véritable photo.

Commençons par en récupérer une sur le web avec `urlretrieve(adresse, nouveau nom)` :

```python
from urllib.request import urlretrieve

urlretrieve("https://scandallos.files.wordpress.com/2015/04/ada-lovelace-portrait.jpg", "ada.jpg")
```

![ada](./IMG/ada.jpg)


et transformons la en tableau (matrice) Python :

```python
ada0 = plt.imread("ada.jpg").tolist()
```

Le problème est que chaque pixel est ici un triplet `[R, V, B]` où R, V et B ont
la même valeur ( d'où la couleur grise ).


```python
In [533]: ada[0][0]
Out[533]: [218, 218, 218]
```


Nous allons donc remplacer chaque
pixel `[N, N, N]` par l'entier `N` :

```python
ada = [[px[0] for px in ligne] for ligne in ada0]
```

Maintenant :

```python
In [534]: ada[0][0]
Out[534]: 218
```


>**Énigme** :  si mat  est une  matrice représentant  une image,  que représente
>`mat[i][j]` ?


Expliquez ce qui se passe ici :

```python
image = [ [255 - px for px in ligne] for ligne in ada]
```

ou de manière équivalente :

```python
nb_lignes = len(ada)
nb_cols = len(ada[0])

image = [ [255 - ada[i][j] for j in range(nb_cols)] for i in range(nb_lignes)]
```


![ada_neg](./IMG/ada_neg.png)


**À vous de jouer**

Comment obtenir:



![ada_transpo](./IMG/ada_transpo.png)




![ada_rev](./IMG/ada_rev.png)





![ada_mir](./IMG/ada_mir.png)




![ada_fonc](./IMG/ada_fonc.png)



![ada_clair](./IMG/ada_clair.png)


#### Bruit

![ada_bruit](./IMG/ada_bruit.png)


Comment atténuer le *bruit* sur cette image ? 
##
## Contour


Pour sélectionner des objets sur une image, on peut essayer de
détecter leurs bords, i.e. des zones de brusque changement de niveaux
de gris.

On remplace alors un pixel `im[i][j]` de l'image `im` par une mesure des écarts des voisins
immédiats donnée par exemple par :

$`\sqrt{ \left(\mathtt{im[i][j+1]}-\mathtt{im[i][j-1]}\right)^2+ \left(\mathtt{im[i+1][j]}-\mathtt{im[i-1][j]}\right)^2}`$


![ada](./IMG/ada_cont.png)


ou `max(im[i][j-1] - im[i][j+1] , im[i-1][j] - im[i+1][j])`


![ada](./IMG/ada_cont2.png)

ou,  en étant  plus  binaire (en  s'inspirant  de la  formule  précédente et  en
pensant *seuil*)



![ada](./IMG/ada_cont3.png)
