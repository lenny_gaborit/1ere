# TP Conditionnelles


## Pile ou face

Voici l'aide de `randint` du module `random`:

```console
Help on method randint in module random:

randint(a, b)
    Return random integer in range [a, b], including both end points.
```


Créez une fenêtre `easygui` qui demande de choisir Pile ou Face.  On "lance" alors une
pièce.
Si on tombe sur  Pile, on annonce que le joueur aura son  Bac, et sinon qu'il ne
l'aura pas.


## Année bissextile

Renseignez-vous  sur l'ajustement  du calendrier  grégorien et  la création  des
années bissextiles.

On peut regarder dans son terminal sur wikipedia si on installe quelques outils:

```console
$ sudo apt-get install nodejs
$ sudo apt-get install npm
$ sudo npm install wikit -g
```

Et ensuite :

```console
$ wikit Gregorian calendar

 The Gregorian calendar is the calendar used in most of the world. It is named after
 Pope Gregory XIII, who introduced it in October 1582. The calendar spaces leap years
 to make the average year 365.2425 days long, approximating the 365.2422-day tropical
 year that is determined by the Earth's revolution around the Sun. The rule for leap
 years is: Every year that is exactly divisible by four is a leap year, except for
 years that are exactly divisible by 100, but these centurial years are leap years
 if they are exactly divisible by 400. For example, the years 1700, 1800, and 1900
 are not leap years, but the year 2000 is. The calendar was developed as a correction
 to the Julian calendar, shortening the average year by 0.0075 days to stop the drift
 of the calendar with respect to the equinoxes. To deal with the 10 days' difference
 (between calendar and reality) that this drift had already reached, the date was
 advanced so that 4 October 1582 was followed by 15 October 1582. There was no discontinuity
 in the cycle of weekdays or of the Anno Domini calendar era. The reform also altered
 the lunar cycle used by the Church to calculate the date for Easter (computus),
 restoring it to the time of the year as originally celebrated by the early Church.
 The reform was adopted initially by the Catholic countries of Europe and their overseas
 possessions. Over the next three centuries, the Protestant and Eastern Orthodox
 countries also moved to what they called the Improved calendar, with Greece being
 the last European country to adopt the calendar in 1923. To unambiguously specify
 a date during the transition period, (or in history texts), dual dating is sometimes
 used to specify both Old Style and New Style dates (abbreviated as O.S and N.S.
 respectively). Due to globalization in the 20th century, the calendar has also been
 adopted by most non-Western countries for civil purposes. The calendar era carries
 the alternative secular name of "Common Era".
```

Créez une petite appli `easygui` qui demande  de rentrer une année et qui répond
si l'année est bissextile ou non.

## Delta

Créez une appli `easygui` qui demande de rentrer une équation du second degré et
renvoie ses éventuelles solutions :)

Les non-mathématiciens se mettront en équipe avec un(e) mathématicien(ne)...




## Météo

Voici une idée pour  afficher la météo d'une ville donnée.  On aurait pu 'ecrire
une cascade de `if` et pourtant il n'y en a pas : où aurait pu-t-on les mettre ?
Qu'en pensez-vous ?

```python
import easygui as eg
import os
import sys

encore: bool = True
while encore:
    ville: str = eg.enterbox('Ville ?', default='')
    os.system(f'wget -O {ville}.txt https://fr.wttr.in/{ville}?format="%l:+%C+%t+%p+%w"')
    temps: str = open(f'{ville}.txt','r').read()
    tps: str = temps.split()[1]
    logo: str = f'{tps}.png'
    os.system(f'rm {ville}.txt')
    encore = eg.ynbox(msg=temps, choices=['Une autre ville ?','Quitter'], image=logo, title=f'Météo de {ville}')
```


