# Instructions et expressions conditionnelles


Une fonction  renvoyant un  booléen permet de  répondre par Vrai  ou Faux  à une
question.


```mermaid
graph LR;
A[Question]-->C{Réponse à la question};
C-->|Vraie| D[Faire/Renvoyer ceci];
C-->|Fausse| E[Faire/Renvoyer cela];
```

* Si on *fait* quelque chose, on parle d'**instruction** conditionnelle.

* Si on *renvoie* une valeur, on parle d'**expression** conditionnelle.


Par exemple, avec `easygui`, la fonction  `ynbox` propose deux boutons à cliquer
et renvoie vrai si on clique sur le premier bouton et faux sinon.

On veut renvoyer  la photo de James Bond  si on clique sur `Oui` et  la photo de
Trinity si on clique sur `Non`.

On peut alors donner deux versions de la conditionnelle:

* Version **Instruction conditionnelle**:

```python
import easygui as eg

imtri:str    = "trinity.jpg"
imbond:str   = "bond.jpg"
question:str = "Êtes-vous pour ?"
msg:str      = "Bien joué !"
titre:str    = "Sondage"
choix        = "Oui", "Non"


rep:bool = eg.ynbox(question, titre, choix)

if rep:
    eg.msgbox(msg, image = imbond)
else:
    eg.msgbox(msg, image = imtri)
```

* Version **Expression conditionnelle**:

```python
import easygui as eg

imtri:str    = "trinity.jpg"
imbond:str   = "bond.jpg"
question:str = "Êtes-vous pour ?"
msg:str      = "Bien joué !"
titre:str    = "Sondage"
choix:str    = "Oui", "Non"

rep:bool = eg.ynbox(question, titre, choix)

eg.msgbox(msg, image = imtri if rep else imbond)
```


Plus terre, voyons un calcul de maximum:

* **Instruction conditionnelle**:

```python
if x > y:
	maxi = x
else:
	maxin = y
```

* **Expression conditionnelle**:

```python
maxi = x if x > y else y
```

## `elif`

Parfois, les choix ne sont pas binaires. On doit distinguer plus de deux cas.


```python
choix3   = "Oui", "Non", "Chais pas"
rep3:str = eg.buttonbox(question, titre, choices = choix3)
msg3:str = "Looser"

if rep3 == "Oui":
    eg.msgbox(msg, image = imbond)
elif rep3 == "Non":
    eg.msgbox(msg, image = imtri)
else:
    eg.msgbox(msg3)
```

On peut aussi avoir une version "Expression":

```python
eg.msgbox(msg3 if rep3 == 'Chais pas' else msg,
	image = imbond if rep3 == 'Oui' else imtri if rep3 == 'Non' else None)
```

