---
date: '7 Novembre 2019'
title: Test d'informatique - 1ère - Sujet A
---
# Arborescence

Voici une arborescence:

![arbo1](Diag1.png)


1. Donnez les  commandes qui permettent de créer cette  arborescence sachant que
   vous disposez au départ des répertoires `/`, `home`, `usr`, `tmp`, `Joe` et `Izzy`.
2. Créez deux fichiers vides appelés 'un' et 'deux' dans le home.
3. Copier le fichier `un` dans le répertoire `B` en lui donnant le nom `trois`.
4. Comment  réaliser la copie précédente  en utilisant un chemin  **relatif** si
   vous êtes:
   1. dans le répertoire `A` ?
   2. dans le répertoire `B` ?
   3. Dans le répertoire `C` ?
5. Comment renommer le fichier `trois` en `quatre` ?
6. On se trouve dans le répertoire `8`. Donnez la commande qui permet d'aller
   dans le répertoire  `6` en suivant un  chemin **relatif**, puis  en suivant un
   chemin **absolu**.
7. On  se trouve dans  le répertoire `3`. Donnez  les commandes qui  permettent de
   supprimer le répertoire `C`.


# Droits


Voici des renseigenments sur des fichiers d'un certain répertoire:

```console
joe@moi:~/Test$ ls -l
total 16
-rwx------ 10 joe  rugby   25 Oct 13 23:48 fichier1
-rw-r--r-- 45 bill etu     19 Oct 13 23:48 fichier2
-rwx--x--x 54 amy  adm     95 Oct 13 23:48 fichier3
-r-xr----- 13 izzy staff  118 Oct 13 23:49 fichier4
```

1. Donner, pour chaque fichier:
   1. son nom
   2. le nom de son propriétaire
   3. les droits du propriétaire
   4. les droits du groupe
   5. La taille du fichier
2. Donner en octal les droits de chaque fichier.
3. Donner le droit de lecture au groupe pour `fichier4`
4. Enlever le droit d'exécution aux autres pour `fichier3`


# Flux, redirections et scripts

1.  Donner une  ligne de  commande qui  permet d'écrire,  dans un  fichier nommé
   `rep.txt`, tous les noms de fichiers présents dans le répertoire courant.
   
2. Écrire  un script  `bash` qui demande  à l'utilisateur de  rentrer un  nom de
   répertoire et qui écrit les noms  des répertoires présents dans ce répertoire
   dans  un fichier  `rep_nom_du_répertoire.txt` où  `nom_du_répertoire` est  en
   fait le nom du répertoire entré par l'utilisateur.
   
3.  Le script  précédent est  enregistré sous  le nom  `les_reps.sh`. Comment
	l'utiliser dans un  terminal ? Y a-t-il une manipulation  à faire avant de
	pouvoir l'exécuter ? Laquelle ?
	
4.  Donner  ce  qui  est  affiché   après  la  dernière  ligne  de  cette  suite
   d'instructions `bash` en justifiant la réponse:
   
```console
joe@moi:~$ echo Bonjour > fic.txt 
joe@moi:~$ echo Adieu > fic.txt
joe@moi:~$ cat < fic.txt | rev | cowsay
```

5. Expliquer ce que fait le script suivant:

```bash
#!/bin/bash

    D=$1
    NB=0
    while [ "$D" -gt "$NB" ]; do
        echo -n " $D"
        D=$((D-1))
    done
```
   

# Codage des entiers

1. À quel entier relatif correspond le nombre codé sur un octet `1011 0101` ?

2. Quelle la représentation en complément à 2 de 37 sur 8 bits ? Sur 16 bits ?

3. Quelle la représentation en complément à 2 de 137 sur 8 bits ? Sur 16 bits ?

# Codage des caractères

Voici la table ASCII étendue:


|       | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | A   | B   | C   | D   | E   | F   |
|:-----:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
| **2** |     | !   | "   | #   | $   | %   | &   | '   | (   | )   | *   | +   | ,   | -   | .   | /   |
| **3** | 0   | 1   | 2   | 3   | 4   | 5   | 6   | 7   | 8   | 9   | :   | ;   | <   | =   | >   | ?   |
| **4** | @   | A   | B   | C   | D   | E   | F   | G   | H   | I   | J   | K   | L   | M   | N   | O   |
| **5** | P   | Q   | R   | S   | T   | U   | V   | W   | X   | Y   | Z   | [   | \   | ]   | ^   | _   |
| **6** | `   | a   | b   | c   | d   | e   | f   | g   | h   | i   | j   | k   | l   | m   | n   | o   |
| **7** | p   | q   | r   | s   | t   | u   | v   | w   | x   | y   | z   | {   |     | }   | ~   |     |
| **8** |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| **9** |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
| **A** |     | ¡   | ¢   | £   | ¤   | ¥   | ¦   | §   | ¨   | ©   | ª   | «   | ¬   | -   | ®   | ¯   |
| **B** | °   | ±   | ²   | ³   | ´   | µ   | ¶   | ·   | ¸   | ¹   | º   | »   | ¼   | ½   | ¾   | ¿   |
| **C** | À   | Á   | Â   | Ã   | Ä   | Å   | Æ   | Ç   | È   | É   | Ê   | Ë   | Ì   | Í   | Î   | Ï   |
| **D** | Ð   | Ñ   | Ò   | Ó   | Ô   | Õ   | Ö   | ×   | Ø   | Ù   | Ú   | Û   | Ü   | Ý   | Þ   | ß   |
| **E** | à   | á   | â   | ã   | ä   | å   | æ   | ç   | è   | é   | ê   | ë   | ì   | í   | î   | ï   |
| **F** | ð   | ñ   | ò   | ó   | ô   | õ   | ö   | ÷   | ø   | ù   | ú   | û   | ü   | ý   | þ   | ÿ   |


1. Quelle est le code hexadécimal de `È` en Latin-1 ? En UTF-8 ?

2. Coder en UTF-8 puis en Latin-1 `Ô l'été`

3. Décoder `54 6F 75  74 20 6C 27 C3 A9 74 C3 A9` sachant  que ce texte est codé
   en Latin 1.
   
   Faites de même sachant qu'il est codé en UTF8.
   
   
# Opérations bit à bit


1. Complétez le tableau suivant en évaluant les opérations proposées à partir des
octets `x` et `y` fournis:

|Propriété | Signification|
|:---------|:------------:|
| `x` | `01101001`|
| `y` | `01010101`|
| `x & y` | |
| `x \| y` | | 
| `x ^ y`| |
| `x ↑ y`| |

2. Quel est le résultat de l'opération `(101101 >> 2) & 0xE` ?

