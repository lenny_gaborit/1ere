
## Déterminer des fonctions basiques 

1. Déterminer une fonction qui calcule la **cardinalité** d'une table, c'est-à-dire son nombre de lignes.

2. Déterminer une fonction qui renvoie la liste des **attributs** d'une table.




<!--
  ## Correction
  1. On peut utiliser la fonction `len`:
  
  ```python
  def cardinalité(table):
    """
    La cardinalité est le nombre de lignes d'une table
    """
    return len(table)
  ```
  
  2. On peut uiliser la méthode `keys` qui renvoie l'ensemble des clés d'un dictionnaire
  
  ```python
  def attributs(table):
    """
    renvoie la liste des attributs d'une table
    """
    return list(table[0].keys())
  ```
  
  On peut aussi parcourir une ligne donnée de la table. On fixe le numéro de ligne par
  défaut à 0.
  
  ```python
  def attributs2(table, no_ligne=0):
    return [attribut for attribut in table[no_ligne]]
  ```
  
  
-->


## Reconnaître une fonction 

Quel est le rôle de la fonction suivante:

```python
def mystère(t, cs):
    t_p = []
    for l in t:
        new_l = {}
        for c in l:
            if c in cs:
                new_l[c] = l[c]
        t_p.append(new_l)
    return t_p
```



<!--
  ## Correction

C'est une version écrit sans compréhension  de liste de la fonction `projection`
introduite  dans la  fiche  17. On  peut  réécrire `mystère`  avec  des noms  de
variables moins...mystèrieux:

```python
def projection(table, liste_clés):
    table_proj = []
    for ligne in table:
        new_line = {}
        for clé in ligne:
            if clé in liste_attributs:
                new_line[clé] = ligne[clé]
        table_proj.append(new_line)
    return table_proj
```

qui a exactement le même rôle que:

```python
def projection(table, liste_clés):
    return [{clé:ligne[clé] for clé in ligne if clé in liste_clés} for ligne in table]
```

Que préférez-vous ;) ?

-->


## Tester la cohérence d'une table

1. Déterminer une fonction `coherence_attributs(table)` qui teste si chaque ligne a le même ensemble d'attributs.

2. Déterminer une fonction `existe_doublons(table, attribut_ref)` qui vérifie si un attribut de référence apparaît deux fois avec
    la même valeur dans une table.



<!--
  ## Correction
  
  1. On  teste si chaque  ligne a le même  ensemble d'attributs en  utilisant la
     fonction `attributs2(table, no_ligne)` de l'exercice précédent.
  ```python
     def coherence_attributs(table):
    ref = attributs2(table) # ensemble des attributs de la 1ère ligne
    no_ligne = 1
    card = cardinalité(table)  # nombre de lignes de la table
    while no_ligne < card and attributs2(table, no_ligne) == ref :
        # on avnce d'une ligne tant que les attributs sont les mêmes et
        # qu'on ne dépasse pas la taille de la table
        no_ligne += 1
    # si la table est cohérente `no_ligne` vaut la cardinalité de la table
    return no_ligne == card    
  ```
  
  2. On utilise le fait qu'il n'existe pas de doublon dans un ensemble. On forme
     donc l'ensemble des  valeurs prises par l'attribut de référence.  Il y a un
     doublon si  la longueur  de cet  ensemble est  inférieure à  la cardinalité
     de la table.
  ```python
  def existe_doublons(table, attribut_ref):
    ens_ref = {ligne[attribut_ref] for ligne in table}
    return len(ens_ref) != cardinalité(table)
  ```
  
-->






## Lier tableur, fichier csv et liste de dictionnaires 

On dispose de la liste de dictionnaires suivante:

```python
BaseAliens = 
[{'NomAlien': 'Zorglub', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': 1},
 {'NomAlien': 'Blorx', 'Sexe': 'M', 'Planete': 'Euterpe', 'NoCabine': 2},
 {'NomAlien': 'Urxiz', 'Sexe': 'M', 'Planete': 'Aurora', 'NoCabine': 3},
 {'NomAlien': 'Zbleurdite', 'Sexe': 'F', 'Planete': 'Trantor', 'NoCabine': 4},
 {'NomAlien': 'Darneurane', 'Sexe': 'M', 'Planete': 'Trantor', 'NoCabine': 5},
 {'NomAlien': 'Mulzo', 'Sexe': 'M', 'Planete': 'Helicon', 'NoCabine': 6},
 {'NomAlien': 'Zzzzzz', 'Sexe': 'F', 'Planete': 'Aurora', 'NoCabine': 7},
 {'NomAlien': 'Arghh', 'Sexe': 'M', 'Planete': 'Nexon', 'NoCabine': 8},
 {'NomAlien': 'Joranum', 'Sexe': 'F', 'Planete': 'Euterpe', 'NoCabine': 9}]

```

On travaille avec le tableur `LibreOffice Calc` de la suite `LibreOffice` qui produit des fichiers
au format `odt`  ( alors que le  tableur `Excel` de la  suite `Microsoft Office`
produit des fichiers au format `xlsx`)

1. Quelle est la première ligne de la feuille de calcul obtenue dans un tableur
   à partir de cette liste?
   
2. Quelle commande lancer pour obtenir le fichier CSV correspondant?

3. Quelle est la deuxième ligne du fichier CSV correspondant?

4.  Quelle valeur trouve-t-on à la cellule `C8` de la feuille de calcul
   correspondante?
   
5. Par quelle commande obtient-on cette valeur à partir de la liste `BaseAliens`?

6. Une erreur de  saisie s'est produite: Joranum provient en  fait de la planète
   Aurora. Quelle  commande exécuter pour  modifier le fichier  correspondant du
   tableur? 

<!--
 ## Correction

 1. La première ligne correspond aux clés de chaque dictionnaire.

  -------------------------------------------
  |1 | NomAlien | Sexe | Planete | NoCabine |
  -------------------------------------------
  
 2. Il faut exécuter 
    ``` python
    >>> vers_csv('BaseAliens',['NomAlien', 'Sexe', 'Planete', 'NoCabine'])
	```
  
 3. La deuxième ligne  du fichier CSV correspond au premier  élément de la liste
    `BaseAliens`. Seules  les valeurs  apparaissent et  elles sont  séparées par
    des virgules:
	
	`Zorglub,M,Trantor,1`
  
 4.  Il s'agit  de la  valeur correspondant  à la  clé en  3e position  (`C`) donc
    `Planete` du huitième dictionnaire (`8`). Le résultat est donc `Nexon`
	
 5. La huitième ligne correspond à l'élément numéro 7 de la liste: 
 `BaseAliens[7]['Planete']`
 
 6. Il  faut d'abord modifier  la liste `BaseAliens`.  Si l'on sait  que Joranum
    correspond au dernier dictionnaire de la liste, on peut faire directement:
 
 ```python
 >>> BaseAliens[-1]['Planete'] = 'Aurora'
 ```
 
 On  peut sinon  rechercher la  ligne où  apparait Joranum  puis la  modifier en
 conséquence:
 
 ```python
 for alien in BaseAliens:
    if alien['NomAlien'] == 'Joranum':
        alien['Planete'] = 'Aurora'
 ```

Il reste dans chaque cas à créer le fichier CSV correspondant:

    ``` python
    >>> vers_csv('BaseAliens',['NomAlien', 'Sexe', 'Planete', 'NoCabine'])
	```
-->


## Rajouter une ligne ou une colonne à une table

On dispose de la table suivante:

```
   Nom Anglais Info Maths
0  Joe      17   18    16
1  Zoé      15   17    19
2  Max      19   13    14
```

au format CSV dans le répertoire courant sous le nom `'./Groupe1.csv'`.

1. Comment  obtenir la  liste de dictionnaires  correspondante en  utilisant une
   fonction introduite dans le cours ?
 
2. Ajouter les notes  de l'élève Rose qui  a eu 17 en  Maths, 18 en
   Info et 19 en Anglais.

3.  On voudrait  ajouter une  colonne  contenant les  moyennes de  chaque élève 
	afin d'obtenir le tableau suivant:
   
```
    Nom Anglais Info Maths Moyenne
0   Joe      17   18    16    17.0
1   Zoé      15   17    19    17.0
2   Max      19   13    14    15.3
3  Rose      19   18    17    18.0
```

On doit renvoyer une nouvelle table qui ne modifie pas la table d'origine. Pour effectuer une copie 
d'une  liste  d'objets complexes  (ici  une  liste  de dictionnaires),  on  peut
utiliser la  fonction `deepcopy` de la  bibliothèque du même nom.  La fonction à
créer pourra donc avoir la structure suivante qu'il s'agit de compléter:
```python
from deepcopy import deepcopy

def ajoute_moyenne_ligne(table):
   new_table = deepcopy(table)
   ...
   return new_table
```

Pour obtenir l'affichage d'un flottant arrondi  à 2 chiffres après la virgule, on
peut             utiliser             la            méthode             `format`
(https://docs.python.org/3.7/library/string.html#formatexamples)
Par exemple:

```python
>>> f'{314/100:.2f}' # .2f indique un flottant avec 2 chiffres après la virgule
'3.14'
```
 

4. Ajouter une ligne qui contient les moyennes par matières. Vous devez obtenir
   la table suivante:

```
          Nom Anglais  Info Maths
0      Joe      17    18    16
1      Zoé      15    17    19
2      Max      19    13    14
3     Rose      19    18    17
4  Moyenne    17.5  16.5  16.5
```
   

   
   
   
   

<!--
 ## Correction

 1. ```python
    >>> vers_csv('Groupe1', ordre_cols=['Nom','Anglais','Info','Maths'])
	```
 
 2. On ajoute une ligne à la liste `Groupe1`:
 
 ```python
 Groupe1.append({'Nom':'Rose', 'Maths':'17', 'Info':'18', 'Anglais':'19'})
 ```

 3. On parcourt chaque ligne (qui représente un élève) et on ajoute chaque note:
 
  ```python
  def ajoute_moyenne_ligne(table):
     new_table = deepcopy(table) # copie de la table d'origine
     for eleve in new_table: 
         total, nb_notes = 0, 0 # pour chaque ligne on va additionner
                                 # les notes et le nombre de notes 
         for matiere in eleve:
             if matiere != 'Nom': # on ajoute les notes mais pas les noms !
                 total += eval(eleve[matiere]) # les notes sont stockées
                                                # sous forme de strings : il
                                                # faut les évaluer en tant que
                                                # nombres
                 nb_notes += 1
         ligne['Moyenne'] = f"{total / nb_notes:.1f}"
     return new_table
  ```
 
 4. 
 
  ```python
  def ajoute_moyenne_colonne(table):
     """
     Fonction qui ajoute une ligne contenant les moyennes de chaque colonne.
     Ainsi::
        Nom Anglais Info Maths
     0  Joe      17   18    16
     1  Zoé      15   17    19
     2  Max      19   13    14
     devient:
        Nom Anglais  Info Maths
     0  Joe      17    18    16    
     1  Zoé      15    17    19    
     2  Max      19    13    14
     3  Moyenne    17.0  16.0  16.3 
     """
     new_table = deepcopy(table)
     nb_eleves = len(table)
     # On crée une ligne remplie de 0 au départ
     ligne_moy = {matiere: 0 for matiere in table[0].keys() if matiere != 'Nom'}
     for eleve in table:
         for matiere in eleve:
             if matiere != 'Nom':
                 ligne_moy[matiere] += eval(eleve[matiere])
     # On remplace la somme par la moyenne
     for matiere in ligne_moy:
         ligne_moy[matiere] = '{:.1f}'.format(ligne_moy[matiere] / nb_eleves)
     # on donne comme nom 'Moyenne' à cette nouvelle ligne
     ligne_moy['Nom'] = 'Moyenne'
     new_table.append(ligne_moy)
     return new_table
  ```
-->






## Sélectionner, trier, joindre

On dispose des tables suivantes:

`BaseAgents`:

```
   NomAgent VilleAgent
0    Branno   Terminus
1    Darell   Terminus
2  Demerzel        Uco
3    Seldon   Terminus
4   Dornick     Kalgan
5    Hardin   Terminus
6   Trevize   Hesperos
7   Pelorat     Kalgan
8     Riose   Terminus
```

et `BaseGardiens`:

```
   NoCabine  NomAgent
0         1    Branno
1         2    Darell
2         3  Demerzel
3         4    Seldon
4         5   Dornick
5         6    Hardin
6         7   Trevize
7         8   Pelorat
8         9     Riose
```

1. Renvoyer `BaseTerminus`, une table extraite de `BaseAgents` ne contenant que les lignes dont l'attribut `VilleAgent` vaut `Terminus`.

2. Renvoyer `BaseAlpha`, une table dérivée de `BaseAgents` triée selon l'ordre alphabétique du nom des agents.

3. Renvoyer `BaseComplete`, la table contenant le numéro de cabine, la ville et le nom de l'agent.

4. Renvoyer `BaseVille`, la table contenant le numéro de cabine et la ville des agents.

5. Renvoyer `BaseImpair`, la table contenant le nom et la ville des agents ne venant pas de Terminus et dont le numéro de cabine est impair.



<!-- Correction

1. On utilise la fonction `select`:

```python

>>> BaseTerminus = select(BaseAgents, "ligne['VilleAgent'] == 'Terminus'")
>>> BaseTerminus
[{'NomAgent': 'Branno', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Darell', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Seldon', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Hardin', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Riose', 'VilleAgent': 'Terminus'}]
```

2. On utilise la fonction `tri`:

```python
>>> BaseAlpha = tri(BaseAgents, 'NomAgent')

>>> BaseAlpha 
[{'NomAgent': 'Branno', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Darell', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Demerzel', 'VilleAgent': 'Uco'},
 {'NomAgent': 'Dornick', 'VilleAgent': 'Kalgan'},
 {'NomAgent': 'Hardin', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Pelorat', 'VilleAgent': 'Kalgan'},
 {'NomAgent': 'Riose', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Seldon', 'VilleAgent': 'Terminus'},
 {'NomAgent': 'Trevize', 'VilleAgent': 'Hesperos'}]
```

3. On doit effectuer une jointure des deux tables, l'attribut commun étant `NomAgent`:

```python
>>> BaseComplete = jointure(BaseAgents, BaseGardiens, 'NomAgent')

>>> BaseComplete 
[{'NomAgent': 'Branno', 'VilleAgent': 'Terminus', 'NoCabine': 1},
 {'NomAgent': 'Darell', 'VilleAgent': 'Terminus', 'NoCabine': 2},
 {'NomAgent': 'Demerzel', 'VilleAgent': 'Uco', 'NoCabine': 3},
 {'NomAgent': 'Seldon', 'VilleAgent': 'Terminus', 'NoCabine': 4},
 {'NomAgent': 'Dornick', 'VilleAgent': 'Kalgan', 'NoCabine': 5},
 {'NomAgent': 'Hardin', 'VilleAgent': 'Terminus', 'NoCabine': 6},
 {'NomAgent': 'Trevize', 'VilleAgent': 'Hesperos', 'NoCabine': 7},
 {'NomAgent': 'Pelorat', 'VilleAgent': 'Kalgan', 'NoCabine': 8},
 {'NomAgent': 'Riose', 'VilleAgent': 'Terminus', 'NoCabine': 9}]

```


4. On utilise la fonction `projection` sur la table `BaseComplete` précédente:

```python
>>> BaseVille = projection(BaseComplete, ['NoCabine', 'VilleAgent'])

>>> BaseVille
[{'VilleAgent': 'Terminus', 'NoCabine': 1},
 {'VilleAgent': 'Terminus', 'NoCabine': 2},
 {'VilleAgent': 'Uco', 'NoCabine': 3},
 {'VilleAgent': 'Terminus', 'NoCabine': 4},
 {'VilleAgent': 'Kalgan', 'NoCabine': 5},
 {'VilleAgent': 'Terminus', 'NoCabine': 6},
 {'VilleAgent': 'Hesperos', 'NoCabine': 7},
 {'VilleAgent': 'Kalgan', 'NoCabine': 8},
 {'VilleAgent': 'Terminus', 'NoCabine': 9}]
```

5. On commence par une selection suivie d'une projection sur `BaseComplete`:

```python
>>> BaseImpair0 = select(BaseComplete, "ligne['VilleAgent'] != 'Terminus' and ligne['NoCabine'] % 2 == 1")

>>> BaseImpair0
[{'NomAgent': 'Demerzel', 'VilleAgent': 'Uco', 'NoCabine': 3},
 {'NomAgent': 'Dornick', 'VilleAgent': 'Kalgan', 'NoCabine': 5},
 {'NomAgent': 'Trevize', 'VilleAgent': 'Hesperos', 'NoCabine': 7}]

>>> BaseImpair = projection(BaseImpair0, ['NomAgent', 'VilleAgent'])

>>> BaseImpair
[{'NomAgent': 'Demerzel', 'VilleAgent': 'Uco'},
 {'NomAgent': 'Dornick', 'VilleAgent': 'Kalgan'},
 {'NomAgent': 'Trevize', 'VilleAgent': 'Hesperos'}]

```



## Étude complète d'une table de données

Chaque jour, l'organisation des *Hommes En Noir*(HEN) doit gérer les allées et venues
des extraterrestres sur Terre.  En  arrivant, un extraterrestre est confiné dans
une cellule et surveillé par un gardien. 
Pour les aider à s'organiser, les HEN disposent de cinq tables de données résumant
les informations essentielles sur les aliens et les gardiens:


* La table `BaseAliens` qui donne des renseignements sur les extraterrestres:


|  NoCabine |  NomAlien | Planete| Sexe|
|-----|---|---|---|
| 1 | Zorglub | Trantor | M|
| 2 | Blorx | Euterpe | M |
| 3 | Urxiz | Aurora | M |
| 4 | Zbleurdite | Trantor | F|
| 5 | Darneurane | Trantor | M|
| 6 | Mulzo | Helicon | M |
| 7 | Zzzzzz | Aurora | F |
| 8 | Arghh | Nexon | M |
| 9 | Joranum | Euterpe | F |

* La table `BaseAgents` qui donne le nom et la ville d'origine des agents:


|  NomAgent| VilleAgent|
|---|---|
|Branno |  Terminus|
|    Darell |  Terminus|
|  Demerzel |  Arcturus|
|    Seldon|   Terminus|
|   Dornick|     Kalgan|
|    Hardin|   Terminus|
|   Trevize |  Hesperos|
|   Pelorat |    Kalgan|
|     Riose|   Terminus|
|    Palver|    Siwenna|
|    Amaryl   |Arcturus|


* La table `BaseGardiens` qui affecte à chaque cabine un gardien:


|  NoCabine|  NomAgent|
|---|---|
|1|    Branno|
|         2|    Darell|
|         3|  Demerzel|
|         4|    Seldon|
|         5|   Dornick|
|         6|    Hardin|
|         7|   Trevize|
|         8|   Pelorat|
|         9|     Riose|

* La table `BaseMiams` qui donne l'aliment servi à chaque alien:


|       Aliment|    NomAlien|
|:---|---|
|      Bortsch  |   Zorglub|
|       Bortsch |      Blorx|
|       Zoumise |      Urxiz|
|       Bortsch | Zbleurdite|
|  Schwanstucke | Darneurane|
|       Kashpir |      Mulzo|
|       Kashpir |     Zzzzzz|
|       Zoumise |      Arghh|
|       Bortsch |    Joranum|



* La table `BaseCabines` qui précise dans quelle allée se trouve chaque cabine:


|  NoAllee|  NoCabine|
|---|---|
|        1 |        1|
|        1 |        2|
|        2 |        3|
|        1 |        4|
|        2 |        5|
|        2 |        6|
|        2 |        7|
|        1 |        8|
|        1 |        9|


* La table `BaseResponsables` qui précise l'agent responsable de chaque allée:


|  NoAllee| NomAgent|
|---|---|
|        1|   Seldon|
|        2|  Pelorat|



* La table `BaseVilles` qui précise la planète sur laquelle se trouve chaque ville


|  Planete|     Ville|
|---|:---|
|  Trantor|  Terminus|
|  Euterpe|  Arcturus|
|  Helicon|    Kalgan|
|  Euterpe|  Hesperos|
|     Gaia|   Siwenna|


Dans  tout l'exercice  on pourra  utiliser  les fonctions  introduites dans  les
fiches 16 à 18.

### Mettre en forme


1. Comment entrer ces tables afin de pouvoir utiliser les outils mis au point dans les fiches 16, 17 et 18?

### Extraction de données


2. Comment obtenir l'ensemble des gardiens?


3. Comment obtenir l'ensemble des villes d'où sont originaires les gardiens?

4. Comment obtenir l'ensemble des triplets (numéro de cabine, alien, gardien) pour chaque cabine?

5 Comment obtenir l'ensemble de tous les aliens de l'allée 2?

6. Comment obtenir la liste des aliens  dont les gardiens sont originaires de la
   planète Trantor?

7. Comment obtenir l'ensemble des gardiens des aliens féminins qui mangent du bortsch?
   
   
### Tests

8. Existe-t-il  un aliment  qui commencent par  la même lettre  que le  nom du
   gardien qui surveille l'alien qui le mange?
   
9. Est-ce que  tous les aliens qui ont  un 'x' dans leur nom ont  un gardien qui
   vient de Terminus?
   
   

### Un peu d'aide

1. Il y a plusieurs possibilités : ouvrir un logiciel comme LibreOffice ou Excel
   et recopier ces données puis les exporter au format CSV. On peut également, dans un éditeur de texte simple,
   créer des fichiers CSV. Ou on peut rentrer directement les tables comme liste
   de dictionnaires mais c'est un peu plus fastidieux.
   
2. Il  faut parcourir  la table  `BaseGardiens` et  ne garder  que les  noms des
   agents. L'utilisation d'un ensemble défini par compréhension est appropriée.
   
   On peut également  penser à effectuer une projection de  `BaseGardiens` en ne
   gardant que les noms.
   
3. Attention! Tous les agents ne sont  pas des gardiens. Il faut d'abord joindre
   les  deux tables  `BaseGardiens`  et  `BaseAgents` puis  ne  retenir que  les
   villes dans un ensemble défini par compréhension.
   
4.  Les tables  `BaseGardiens`  et  `BaseAliens` ont  en  commun  le numéro  des
   cabines. Il s'agit donc de joindre ces deux tables.
   
5. Il  faut cette fois joindre  les tables `BaseAliens` et  `BaseCabines` et
   sélectionner les  lignes contenant l'allée 2.  On peut utiliser un  '`if`' ou
   bien la fonction `select`.
   
6. Ici  le nombre de  tables impliquées dans la  requête augmente: il  faut lier
   `BaseAliens` et  `BaseGardiens` par  le numéro de  cabine, puis  relier cette
   nouvelle table  à `BaseAgents` par  le nom de  l'agent pour obtenir  la ville
   d'origine du  gardien et  enfin joindre cette  dernière table  à `BaseVilles`
   pour obtenir  la planète d'origine.  Attention! Dans la dernière  jointure le
   nom des attributs désignant les villes sont différents.
   
7. Toujours des jointures (trois) mais cette fois il y a deux conditions à vérifier:
   l'alien est féminin ET l'aliment est le bortsch.

8. Il  faut à  nouveau joindre `BaseMiams`,  `BaseAliens` et  `BaseGardiens`. La
   première lettre d'un mot s'obtient avec `mot[0]`. 
   
   Pour le test en lui-même, on peut former un ensemble de tests et vérifier que
   `True` appartient à cet ensemble ou bien construire une boucle `while`.
   
9. Cette fois on doit vérifier que  notre condition est toujours vraie, donc que
   `False` n'appartient  pas à notre ensemble  de tests. On reprend  la jointure
   effectuée à la question 6. entre `BaseAliens`, `BaseGardiens` et `BaseAgents`.










