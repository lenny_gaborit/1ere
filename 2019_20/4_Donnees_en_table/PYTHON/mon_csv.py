import csv

def depuis_csv(nom_fichier_csv, separateur=',', encodage='utf-8'):
    """
    Crée une liste de dictionnaires, un par ligne.
    La 1ère ligne du fichier csv est considérée comme la ligne des noms des champs
    Par défaut le délimiteur est la virgule et l'encodage utf8
    """
    lecteur = csv.DictReader(open(nom_fichier_csv,'r',encoding=encodage), delimiter=separateur) 
    return [dict(ligne) for ligne in lecteur]

def vers_csv(nom_table, nom_export, ordre_cols):
    """
    Exporte une liste de dictionnaires sous forme d'un
    fichier csv. On rentre le nom de la table sous forme de chaîne.
    On donne l'ordre des colonnes sous la forme d'une liste d'attributs.
    >>> vers_csv(Groupe1, 'newGroupe1', ordre_cols=['Nom','Anglais','Info','Maths'])
    """
    with open(nom_export + '.csv', 'w') as fic_csv:
        ecrit = csv.DictWriter(fic_csv, fieldnames=ordre_cols)
        ecrit.writeheader() # pour le 1ère ligne
        for ligne in nom_table:
            ecrit.writerow(ligne) # lignes ajoutées 1 à 1
    return None
