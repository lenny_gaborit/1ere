###################################
##     ALGOS SUR LES BASES               #
###################################



## 1 Algo de soustraction pour des nombres positifs

def algo_sous(n: int) -> str:
    """
    Renvoie l'écriture de l'entier positif n
    en base 2 sous forme d'une chaîne de caractères

    >>> algo_sous(12)
    '1100'
    """
    assert 0 <= n, "Le nombre doit être un entier positif !"
    # TODO : enlever la limite max 
    pmax = 10 # comment calculer pmax dans le cas général ?
    nb_billes_restantes = n 
    liste_nb_billes_sur_carte = [2**k for k in range(pmax, -1, -1)]
    chaine_bin = ''
    for nb_billes_sur_carte in liste_nb_billes_sur_carte:
        if nb_billes_sur_carte > nb_billes_restantes:
            chaine_bin += '0'
        else:
            chaine_bin += '1'
            nb_billes_restantes -= nb_billes_sur_carte
    return chaine_bin
    


def convert(nb, base):
    assert 1 < base < 37 and nb > 0 and int(nb) == nb, 'la base doit être entre 2 et 36'
    d = [str(k) if k < 10 else chr(55 + k) for k in range(36)]
    n = nb
    res = ''
    while n > 0 :
        res = d[n % base] + res
        n = n // base
    return res
