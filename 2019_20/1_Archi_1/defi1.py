def voit_bin(n: int) -> None:
    """
    Prend un entier positif en argument et crée un fichier pgm
    affichant le code binaire des entiers de 0 à 2^n - 1
    """
    col = n
    row = 2**n - 1
    ch = 'P1\n{} {}\n'.format(col, row)
    for k in range(2**n):
        b = bin(k)[2:]
        b = '0'*(col - len(b)) + b
        ch = ch + ' '.join(b) + '\n'
    print(ch)
    with open('bin{}.pgm'.format(n), "w") as pgm:
        pgm.write(ch)


        
